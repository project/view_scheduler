<?php

function views_scheduler_process_schedule($type){
	//retrieve any scheduled views
	$scheduled_views = views_scheduler_select_views();

	//if there are no scheduled views
	if (!$scheduled_views) return ;
	
	// If there are scheduled views, iterate through it.
	foreach ($scheduled_views as $views) {
				
		$schedules = views_scheduler_select_schedules($type, $views['vschedule_id']);
		
		//Is the scheduled-time <=time(), if so retreive view an carry out actions
		if ( views_scheduler_activate($schedules['next']) ) {
			
			$nodes      = $views['node'];
			$name       = views_get_view($views	['view']);
			$viewresult = views_build_view('items', $name, NULL, 0, $nodes, 0 );

			//retrieve each node one at a time.
			foreach( $viewresult['items'] as $key => $value ){
				$nid=$viewresult['items'][$key]->nid;
				//carry out the actions
				views_scheduler_carry_out_actions($views[vschedule_id],$nid);
			}
			//turn the array into object since some versions of 
			//schedule do not accept arrays
			//$schedules = objectify_an_array($schedules);
			
			//set new schedule time
			schedule_set_next_publication_time($schedules);
			
		}	
	}
}



function objectify_an_array($arg_array) {
	$tmp = new stdClass; // start off a new (empty) object
	foreach ($arg_array as $key => $value) {
		if (is_array($value)) { // if its multi-dimentional, keep going :)
			$tmp->$key = arr2obj($value);
		} else {
			if (is_numeric($key)) { // can't do it with numbers :(
				die("Cannot turn numeric arrays into objects!");
			}
			$tmp->$key = $value;
		}
	}
	return $tmp; // return the object!
}
//to be worked on more later
//this is to allow for people who want more control to be able
//to run scheduled views using actions.
/*
function action_views_scheduler_run($op, $edit = array(), &$node) {
  switch($op) {
    case 'do':
     views_scheduler_cron();
     break;

    case 'metadata':
      return array(
        'description' => t('Run the scheduled view'),
        'type' => t('View'),
        'batchable' => false,
        'configurable' => false,
      );

  }
}
*/
